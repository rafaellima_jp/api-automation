package br.rafael.teste;
import org.json.simple.JSONObject;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import junit.framework.Assert;

public class TC005_DELETE_Request {
	
	@Test
	void getDetails() {
		
		RestAssured.baseURI="https://reqres.in/api/users";
		RequestSpecification httprequest=RestAssured.given();
		
		JSONObject requestParams= new JSONObject();
		requestParams.put("name", "Lucio");
		requestParams.put("job", "Gerente de Projetos");
		
		httprequest.header("Content-Type", "application/json");
		httprequest.body(requestParams.toJSONString());
		
		Response response=httprequest.request(Method.POST);
		
		String responseBody=response.getBody().asString();
		System.out.println("Response Body: "+responseBody);
		
		int statusCode=response.getStatusCode();
		System.out.println("Status Code: "+statusCode);
		Assert.assertEquals(statusCode, 201);
		
		String id = response.jsonPath().get("id");
		String delete = "/"+id;
		//
		
		response=httprequest.request(Method.DELETE,delete);
		
		responseBody=response.getBody().asString();
		System.out.println("Response Body: "+responseBody);
		
		statusCode=response.getStatusCode();
		System.out.println("Status Code: "+statusCode);
		Assert.assertEquals(statusCode, 204);
		
		
	}

}
