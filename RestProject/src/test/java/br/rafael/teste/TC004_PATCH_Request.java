package br.rafael.teste;
import org.json.simple.JSONObject;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import junit.framework.Assert;

public class TC004_PATCH_Request {
	
	@Test
	void getDetails() {
		
		RestAssured.baseURI="https://reqres.in/api/users";
		RequestSpecification httprequest=RestAssured.given();
		
		JSONObject requestParams= new JSONObject();
		requestParams.put("name", "HUMBERTO");
		requestParams.put("job", "DEV");
		
		httprequest.header("Content-Type", "application/json");
		httprequest.body(requestParams.toJSONString());
		
		Response response=httprequest.request(Method.POST);
		
		String responseBody=response.getBody().asString();
		System.out.println("Response Body: "+responseBody);
		
		int statusCode=response.getStatusCode();
		System.out.println("Status Code: "+statusCode);
		Assert.assertEquals(statusCode, 201);
		
		String id = response.jsonPath().get("id");
		String patch = "/"+id;
		//
		JSONObject requestParamsUpdate= new JSONObject();
		requestParamsUpdate.put("name", "Humberto Lima");
		requestParamsUpdate.put("job", "Desenvolvedor");
		
		httprequest.header("Content-Type", "application/json");
		httprequest.body(requestParamsUpdate.toJSONString());
		
		response=httprequest.request(Method.PATCH,patch);
		
		responseBody=response.getBody().asString();
		System.out.println("Response Body: "+responseBody);
		
		statusCode=response.getStatusCode();
		System.out.println("Status Code: "+statusCode);
		Assert.assertEquals(statusCode, 200);
		
		
	}

}
