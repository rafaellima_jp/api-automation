package br.rafael.teste;
import org.json.simple.JSONObject;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import junit.framework.Assert;

public class TC002_POST_Request {
	
	@Test
	void getDetails() {
		
		RestAssured.baseURI="https://reqres.in/api/users";
		RequestSpecification httprequest=RestAssured.given();
		
		JSONObject requestParams= new JSONObject();
		requestParams.put("name", "Rafael");
		requestParams.put("job", "QA");
		
		httprequest.header("Content-Type", "application/json");
		httprequest.body(requestParams.toJSONString());
		
		Response response=httprequest.request(Method.POST);
		
		String responseBody=response.getBody().asString();
		System.out.println("Response Body: "+responseBody);
		
		int statusCode=response.getStatusCode();
		System.out.println("Status Code: "+statusCode);
		Assert.assertEquals(statusCode, 201);
	}

}
