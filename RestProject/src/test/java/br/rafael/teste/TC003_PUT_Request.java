package br.rafael.teste;
import org.json.simple.JSONObject;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import junit.framework.Assert;

public class TC003_PUT_Request {
	
	@Test
	void getDetails() {
		
		RestAssured.baseURI="https://reqres.in/api/users";
		RequestSpecification httprequest=RestAssured.given();
		
		JSONObject requestParams= new JSONObject();
		requestParams.put("name", "Thales");
		requestParams.put("job", "QA_Junior");
		
		httprequest.header("Content-Type", "application/json");
		httprequest.body(requestParams.toJSONString());
		
		Response response=httprequest.request(Method.POST);
		
		String responseBody=response.getBody().asString();
		System.out.println("Response Body: "+responseBody);
		
		int statusCode=response.getStatusCode();
		System.out.println("Status Code: "+statusCode);
		Assert.assertEquals(statusCode, 201);
		
		String id = response.jsonPath().get("id");
		String put = "/"+id;
		//
		JSONObject requestParamsUpdate= new JSONObject();
		requestParamsUpdate.put("name", "Thales Moura");
		requestParamsUpdate.put("job", "QA");
		
		httprequest.header("Content-Type", "application/json");
		httprequest.body(requestParamsUpdate.toJSONString());
		
		response=httprequest.request(Method.PUT,put);
		
		responseBody=response.getBody().asString();
		System.out.println("Response Body: "+responseBody);
		
		statusCode=response.getStatusCode();
		System.out.println("Status Code: "+statusCode);
		Assert.assertEquals(statusCode, 200);
		
		
	}

}
