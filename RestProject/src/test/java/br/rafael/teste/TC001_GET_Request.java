package br.rafael.teste;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.Method;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import junit.framework.Assert;

public class TC001_GET_Request {
	
	@Test
	void getDetails() {
		
		RestAssured.baseURI="https://reqres.in/api/users/2";
		RequestSpecification httprequest=RestAssured.given();
		Response response=httprequest.request(Method.GET);
		
		String responseBody=response.getBody().asString();
		System.out.println("Response Body: "+responseBody);
		
		int statusCode=response.getStatusCode();
		System.out.println("Status Code: "+statusCode);
		Assert.assertEquals(statusCode, 200);
	}

}
